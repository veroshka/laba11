#include <meters.hpp>
#include <ctime>

namespace vs
{
	Meters::Meters(std::string name, int adress)
	{
		n_name = name;
		n_adress = adress;
	}

	Meters::~Meters() {};

	void Meters::poll()
	{
		srand(time(NULL));
		if (n_adress == 1)
		{
			n_current = (rand() % 5) + 1;
			n_voltage = 3 / n_current;
		}
		else if (n_adress == 2)
		{
			n_current = (rand() % 10) + 1;
			n_voltage = 6 / n_current;
		}
	}

	void Meters::print()
	{
		std::cout << "Name: " << n_name << std::endl;
		std::cout << "Adress: " << n_adress << std::endl;
		std::cout << "Current: " << n_current << std::endl;
		std::cout << "Voltage: " << n_voltage << std::endl;
	}
}