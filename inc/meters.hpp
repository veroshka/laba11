#pragma once
#include <iostream>

namespace vs
{
	class Meters
	{
	public:
		Meters(std::string name, int adress);
		~Meters();
		void poll();
		void print();
	private:
		std::string n_name;
		int n_adress;
		float n_voltage, n_current;
	};
}