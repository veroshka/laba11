#include <iostream>
#include <meters.hpp>
#include <vector>

int main()
{
	std::vector<vs::Meters*> data;

	vs::Meters* meter = new vs::Meters("Energomera_CE308", 1);
	data.push_back(meter);

	meter = new vs::Meters("Mercurii_234", 2);
	data.push_back(meter);

	for (auto& d : data)
	{
		d->poll();
		d->print();
	}

	for (int i = 0; i < data.size(); i++)
	{
		delete data[i];
	}
	return 0;
}
